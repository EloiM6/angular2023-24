import {Component, OnInit} from '@angular/core';
import {SumaService} from "../../shared/services/suma.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-exemple2bis',
  templateUrl: './exemple2bis.component.html',
  styleUrl: './exemple2bis.component.css'
})
export class Exemple2bisComponent implements OnInit{
  resultat !: number;
  sumForm !: FormGroup;

  constructor(private suma:SumaService) {

  }

  ngOnInit():void {
    this.sumForm = new FormGroup({
        num1:new FormControl(0),
        num2:new FormControl(0),
      }

    )
  }
  Calcular():void{
    this.resultat = this.suma.Suma(this.sumForm.controls['num1'].value,this.sumForm.controls['num2'].value)
  }
}
