import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Exemple2bisComponent } from './exemple2bis.component';

describe('Exemple2bisComponent', () => {
  let component: Exemple2bisComponent;
  let fixture: ComponentFixture<Exemple2bisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Exemple2bisComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Exemple2bisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
