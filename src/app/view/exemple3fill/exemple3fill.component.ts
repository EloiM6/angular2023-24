import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-exemple3fill',
  templateUrl: './exemple3fill.component.html',
  styleUrl: './exemple3fill.component.css'
})
export class Exemple3fillComponent {
  @Input() dadafill!:string;
  @Output() dadafillChange : EventEmitter<string> = new EventEmitter<string>();

  CanviText():void{
    this.dadafill="Estic canviant el text";
    this.dadafillChange.emit(this.dadafill);
  }

}
