import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Exemple3fillComponent } from './exemple3fill.component';

describe('Exemple3fillComponent', () => {
  let component: Exemple3fillComponent;
  let fixture: ComponentFixture<Exemple3fillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Exemple3fillComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Exemple3fillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
