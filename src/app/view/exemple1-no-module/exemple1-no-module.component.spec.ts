import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Exemple1NoModuleComponent } from './exemple1-no-module.component';

describe('Exemple1NoModuleComponent', () => {
  let component: Exemple1NoModuleComponent;
  let fixture: ComponentFixture<Exemple1NoModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Exemple1NoModuleComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Exemple1NoModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
