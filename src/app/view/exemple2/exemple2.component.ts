import { Component } from '@angular/core';
import {SumaService} from "../../shared/services/suma.service";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-exemple2',
  templateUrl: './exemple2.component.html',
  styleUrl: './exemple2.component.css'
})
export class Exemple2Component {
  resultat !:number;
  constructor(private suma:SumaService, private form : FormBuilder) {}
  formulario : FormGroup = this.form.group ({
    num1:[0],
    num2:[0],
  })
  Calcular():void{
    this.resultat = this.suma.Suma(this.formulario.controls['num1'].value,this.formulario.controls['num2'].value)
  }
}
