import {Component, Input, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-exemple3',
  templateUrl: './exemple3.component.html',
  styleUrl: './exemple3.component.css'
})
export class Exemple3Component implements OnInit{


  estil : Record<string, string> = {
    'background':'lime',
    'color' : 'hotpink'
  }
  // @Input() = defineProps en vue
  @Input() dada:string='';
  data : FormControl = new FormControl('', [Validators.required ,Validators.minLength(4)]);
  ngOnInit():void {
    this.data.valueChanges.subscribe(nuevoValor=>{
      this.dada=nuevoValor;
    })
  }

  dataFillCh(message:string):void{
    this.dada = message;
    this.data.setValue(this.dada);
  }

  Reset():void{
    this.data.setValue('');
  }
}
