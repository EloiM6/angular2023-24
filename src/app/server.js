const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser=require ('body-parser');
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended:true
}));

// S'estableix un servidor que escolta pel port 3000
app.listen(3000, function (){
  console.log('Aplicació nodejs aixecada!!!');
});

// Es fa una API amb la url / que retorna un json
app.get('/', function (req, res){
  return res.send({error:false, message:"Hello"});
})

// Es defineix els paràmetres per fer operacions CUD contra mongodb
const client = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'albums';
const collection = 'songs'

// Es fa una API amb una resposta de totes les cançons de la col·lecció de songs
app.get('/songs',async (req,res)=>
  {
    let client2;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      const songs = await db.collection(collection).find().toArray();
      res.json(songs);
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);

// Es fa una API amb una resposta amb un filtre
app.get('/songs/:reproduccions/:recaptacio',async (req,res)=>
  {
    let client2;
    let reproduccions = Number(req.params.reproduccions);
    let recaptacio = Number(req.params.recaptacio);
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      // toArray() -> Això és per retornar tots els elements coincidents
      const songs = await db.collection(collection).
      find({'Reproduccions':{$gte:reproduccions}, 'Recaptacio':{$gte:recaptacio}}).toArray();
      // next -> Això és per retornar el primer element elements coicident
      /*const songs = await db.collection(collection).
      find({'Reproduccions':{$gte:reproduccions}, 'Recaptacio':{$gte:recaptacio}}).next();*/
      res.json(songs);
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);
// Exemple de Post
app.post('/songs/insert',async (req,res)=>
  {
    let client2;
    // Les dades es troben el body
    let nom = req.body.nom;
    let reproduccions = Number(req.body.reproduccions);
    let recaptacio = Number(req.body.recaptacio);
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      let id = (await db.collection('songs').find().toArray()).length + 1;
      // Exemple de insert
      const songs = await db.collection(collection).
      insertOne({'id': id,'Nom':nom, 'Reproduccions':reproduccions, 'Recaptacio':recaptacio});

      res.json('Guardat!!!');
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);

