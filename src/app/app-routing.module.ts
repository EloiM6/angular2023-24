import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Exemple1Component} from "./view/exemple1/exemple1.component";
import {Exemple1NoModuleComponent} from "./view/exemple1-no-module/exemple1-no-module.component";
import {Exemple2Component} from "./view/exemple2/exemple2.component";
import {Exemple2bisComponent} from "./view/exemple2bis/exemple2bis.component";
import {Exemple3Component} from "./view/exemple3/exemple3.component";

const routes: Routes = [
  {path: 'Exemple1', component:Exemple1Component },
  {path: 'Exemple1bis', component:Exemple1NoModuleComponent },
  {path: 'Exemple2', component:Exemple2Component },
  {path: 'Exemple2bis', component:Exemple2bisComponent},
  {path: 'Exemple3', component:Exemple3Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
