import { TestBed } from '@angular/core/testing';

import { SumaService } from './suma.service';

describe('SumaService', () => {
  let service: SumaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SumaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
