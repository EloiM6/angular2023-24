import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Exemple1Component } from '../../../view/exemple1/exemple1.component';
import { Exemple2Component } from '../../../view/exemple2/exemple2.component';
import {SumaService} from "../../services/suma.service";
import { Exemple2bisComponent } from '../../../view/exemple2bis/exemple2bis.component';
import {ReactiveFormsModule} from "@angular/forms";
import { Exemple3Component } from '../../../view/exemple3/exemple3.component';
import { Exemple3fillComponent } from '../../../view/exemple3fill/exemple3fill.component';



@NgModule({
  declarations: [
    Exemple1Component,
    Exemple2Component,
    Exemple2bisComponent,
    Exemple3Component,
    Exemple3fillComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports:[Exemple1Component, Exemple2Component, Exemple2bisComponent, Exemple3Component],
  providers:[SumaService]
})
export class ExempleModule { }
